﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class topup : UserControl
    {
        string ID;
        float c1;
        SqlConnection connection;
        public EventHandler OnTopup;

        public topup(string id, SqlConnection connection)
        {
            ID = id;
            this.connection = connection;

            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
        

            string sql3 = "SELECT * from topup where serial = @serial ";
            SqlCommand cmd3 = new SqlCommand(sql3, connection);
            cmd3.Parameters.AddWithValue("serial", textBox1.Text);
            SqlDataReader reader3 = cmd3.ExecuteReader();
            if (reader3.Read())
            {
                MessageBox.Show("เติมเงินเสร็จสิน");
                reader3.Close();
                using (SqlDataReader reader4 = cmd3.ExecuteReader())
                {
                    reader4.Read();
                     c1 = float.Parse(reader4["price"].ToString());
                }
                string sql5 = "DELETE FROM topup WHERE (serial = @Serial)";
                SqlCommand command5 = new SqlCommand(sql5, connection);
                command5.Parameters.Clear();
                command5.Parameters.AddWithValue("Serial", textBox1.Text);
                command5.ExecuteNonQuery();

            }
            else
            {
                MessageBox.Show("รหัสผ่านผิด");
            }

            reader3.Close();

            string sql2 = @"UPDATE customer SET cash = cash + @c1 WHERE Id = @ID";
            SqlCommand command = new SqlCommand(sql2, connection);
              
            command.Parameters.Clear();
            command.Parameters.AddWithValue("c1",c1);
            command.Parameters.AddWithValue("ID",ID);
            command.ExecuteNonQuery();
            string sql = "SELECT * FROM customer WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("id", ID);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                label2.Text = reader["cash"].ToString();
            }
            this?.Invoke(OnTopup, EventArgs.Empty);
        }

        private void Topup_Load(object sender, EventArgs e)
        {
            label1.Text = ID;
            string sql = "SELECT * FROM customer WHERE Id = @id";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("id", ID);
            using (SqlDataReader reader = cmd.ExecuteReader())
            {
                reader.Read();
                label2.Text = reader["cash"].ToString();
            }
        }
    }
}
