﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class Form2 : Form
    {
        public Form2()
        {
            InitializeComponent();
        }
        SqlConnection connection;
        DataSet dataSt;
        private void selectData()
        {
            string sql = "SELECT * FROM customer";
            dataSt = new DataSet();
            using (SqlCommand command = new SqlCommand(sql, connection))
            {
                using (SqlDataAdapter adapter = new SqlDataAdapter(command))
                {
                    adapter.Fill(dataSt, "customer");
                }
            }
        }
        private void Form2_Load(object sender, EventArgs e)
        {
            connection = new SqlConnection(@"Data Source=(LocalDB)\MSSQLLocalDB;AttachDbFilename=|DataDirectory|\Database1.mdf;Integrated Security=True");
            connection.Open();
            this.AcceptButton = button1;
            selectData();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if (textBox1.Text == "" || textBox2.Text == "")
            {
                MessageBox.Show("กรุณกรอกรหัสผ่านให้ครบ");
                return;
            }
            string sql = "SELECT * from customer where Id = @id and password = @password";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("id", textBox1.Text);
            cmd.Parameters.AddWithValue("password", textBox2.Text);
            SqlDataReader reader = cmd.ExecuteReader();
            if (reader.Read())
            {
                    this.Hide();
                    Form1 f2 = new Form1(textBox1.Text);
                    f2.Show();
                    f2.FormClosed += (s, args) => this.Show();    
            }
            else
            {
                MessageBox.Show("รหัสผ่านผิด");
            }
            reader.Close();

        }

    }
}
