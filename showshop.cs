﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Data.SqlClient;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp1
{
    public partial class showshop : Form
    {
        SqlConnection connection;
        string movie, id;
        float c1;
        float c2;
        public EventHandler OnPurchase;
        public showshop(string Movie,string ID,SqlConnection connection)
        {
            this.connection = connection;
            movie = Movie;
            id = ID;
            InitializeComponent();
        }

        private void Button1_Click(object sender, EventArgs e)
        {
            if(c1 < c2)
            {
                MessageBox.Show("Not Enough monney");
            }
            else
            {
                string c3;
                float c4;
                DialogResult result = MessageBox.Show("ยืนยันการซื้อ", "คุณยืนยันที่จะซื้อหรือไม่", MessageBoxButtons.YesNo);
                if (result == DialogResult.Yes)
                {
                    button1.Enabled = false;
                    string sql2 = @"UPDATE customer SET cash = cash - @c1 WHERE Id = @ID";
                    SqlCommand command = new SqlCommand(sql2, connection);
                    command.Parameters.Clear();
                    command.Parameters.AddWithValue("c1", c2);
                    command.Parameters.AddWithValue("ID", id);
                    command.ExecuteNonQuery();

                    string sql = "INSERT INTO customermovie VALUES (@id, @movie)";
                    SqlCommand cmd = new SqlCommand(sql, connection);
                    cmd.Parameters.AddWithValue("id", id);
                    cmd.Parameters.AddWithValue("movie", movie);
                    cmd.ExecuteNonQuery();
                    this?.Invoke(OnPurchase, EventArgs.Empty);
                    c4 = c1 - c2;
                    c3 = c4.ToString();
                    label3.Text = c3;
                }
            }
        }

        private void Showshop_Load(object sender, EventArgs e)
        {
           
            
            pictureBox1.Image = (Image)Properties.Resources.ResourceManager.GetObject(movie);
            string sql2 = "SELECT * FROM customer WHERE Id = @id";
            SqlCommand cmd2 = new SqlCommand(sql2, connection);
            cmd2.Parameters.AddWithValue("id", id);
            using (SqlDataReader reader2 = cmd2.ExecuteReader())
            {
                reader2.Read();
                label4.Text = reader2["name"].ToString();
                label3.Text = reader2["cash"].ToString();
                c1 = float.Parse(reader2["cash"].ToString());
            }

            string sql = "SELECT * FROM movie WHERE seria = @movie";
            SqlCommand cmd = new SqlCommand(sql, connection);
            cmd.Parameters.AddWithValue("movie", movie);
            using (SqlDataReader reader1 = cmd.ExecuteReader())
            {
                reader1.Read();
                label2.Text = reader1["price"].ToString();
                label1.Text = reader1["mname"].ToString();
                textBox1.Text = reader1["about"].ToString();
                c2 = float.Parse(reader1["price"].ToString());
            }
            string sql1 = "SELECT * from customermovie where cusID = @id and movID = @mMovie";
            SqlCommand cmd1 = new SqlCommand(sql1, connection);
            cmd1.Parameters.AddWithValue("id", id);
            cmd1.Parameters.AddWithValue("mMovie", movie);
            SqlDataReader reader = cmd1.ExecuteReader();
            if (reader.Read())
            {
                button1.Enabled = false;
            }
            else
            {
                
            }
            reader.Close();
        }
    }
}
