﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Drawing;
using System.Data;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Data.SqlClient;

namespace WindowsFormsApp1
{
    public partial class owner : UserControl
    {
        string ID;
        int page;
        SqlConnection connection;
        DataSet dataSt;
        Dictionary<string, string> images = new Dictionary<string, string>();
        Dictionary<int, PictureBox> indices = new Dictionary<int, PictureBox>();
        public owner(string id, SqlConnection connection)
        {
            ID = id;
            this.connection = connection;

            InitializeComponent();
        }
        private void updateData()
        {
            string sql = "SELECT  * FROM  customermovie WHERE cusID = @ID ";
            SqlCommand command = new SqlCommand(sql, connection);
            command.Parameters.AddWithValue("id", ID);
            SqlDataAdapter adapter = new SqlDataAdapter(command);
            dataSt = new DataSet();
            adapter.Fill(dataSt, "Movie");
            for (int i = 0; i < dataSt.Tables["Movie"].Rows.Count; i++)
            {
                string serial = dataSt.Tables["Movie"].Rows[i]["movID"].ToString();
                string image = dataSt.Tables["Movie"].Rows[i]["movID"].ToString();
                images.Add(serial, image);
            }
        }
        private void initImage(int page)
        {
            List<string> movies = new List<string>(images.Keys);
            int start = (page - 1) * 4;
            int end = page * 4;
            for (int i = start, j = 0; i < end; i++, j++)
            {
                PictureBox pictureBox = indices[j];
                if (i < movies.Count)
                {
                    string image = images[movies[i]];
                    pictureBox.Image = (Image)Properties.Resources.ResourceManager.GetObject(image);
                }
                else
                {
                    pictureBox.Image = null;
                }
            }
        }
        private int searchIndex(PictureBox p1)
        {
            foreach (int index in indices.Keys)
            {
                PictureBox p2 = indices[index];
                if (p1 == p2)
                {
                    return index;
                }
            }
            return -1;
        }
        private void Button1_Click(object sender, EventArgs e)
        {
            List<string> movies = new List<string>(images.Keys);
            int max = (int)Math.Ceiling((double)movies.Count / 4);
            if (page == 1)
            {
                return;
            }
            else if (page == 2)
            {
                button1.Visible = false;
            }
            page--;
            initImage(page);
            if (page < max)
            {
                button2.Visible = true;
            }

        }

        private void Owner_Load(object sender, EventArgs e)
        {
            indices.Add(0, pictureBox1);
            indices.Add(1, pictureBox2);
            indices.Add(2, pictureBox3);
            indices.Add(3, pictureBox4);
            updateData();
            List<string> movies = new List<string>(images.Keys);
            int max = (int)Math.Ceiling((double)movies.Count / 4);
            page = 1;
            button1.Visible = false;
            if (page == max)
            {
                button2.Visible = false;
            }
            initImage(page);
        }

        private void Button2_Click(object sender, EventArgs e)
        {
            List<string> movies = new List<string>(images.Keys);
            int max = (int)Math.Ceiling((double)movies.Count / 4);
            if (page == max)
            {
                return;
            }
            else if (page == max - 1)
            {
                button2.Visible = false;
            }
            page++;
            initImage(page);
            if (page > 1)
            {
                button1.Visible = true;
            }
        }
        private void PictureBox_Click(object sender, EventArgs e)
        {
            PictureBox pictureBox = (PictureBox)sender;
            if (pictureBox.Image != null)
            {
                int index = searchIndex(pictureBox);
                List<string> movies = new List<string>(images.Keys);
                string movie = movies[(page - 1) * 4 + index];
                string image = images[movie];
                this.Hide();
                showmovie form = new showmovie();
                form.Show();
                form.FormClosed += (s, args) => this.Show();
            }
        }
    }
}
